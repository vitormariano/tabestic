import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Box, Grid, Container } from '@material-ui/core';

const InputGroup = ({ children, title }) => {
  return (
    <Container>
      <Box mb={1}>
        <Grid
          container
          direction="column"
        >
          <Grid item>
            <Typography variant="overline">{title}</Typography>
          </Grid>
          <Grid item>{children}</Grid>
        </Grid>
      </Box>
    </Container>
  );
};

InputGroup.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
};

export default InputGroup;
