import React from 'react';
import Head from 'next/head';
import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';
import { Divider, Typography, Box, TextField, Grid, InputAdornment, FormControl, InputLabel, Select, MenuItem, FormGroup, Checkbox, Button, Container, Table, TableHead, TableRow, TableCell, TableBody, Slide, Slider } from '@material-ui/core';
import { Check, Description, Save, SaveAlt, SettingsInputComponent, ShowChart } from '@material-ui/icons';
import InputGroup from '../components/InputGroup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    actionButton: {
      width: '100%',
      marginBottom: theme.spacing(1),
    },
  })
);

const Home = () => {
  const classes = useStyles({});

  return (
    <>
      <Head>
        <title>TabEstic - Cálculo de Tabelas de Esticamento - Método do Percentual do Vão</title>
      </Head>

      <>
        <Box textAlign="center">
          <Typography variant="h6">TabEstic - Cálculo de Tabelas de Esticamento</Typography>
        </Box>
        <Divider />
        <InputGroup title="Descrição do caso">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={3}>
              <TextField fullWidth label="Subestação" />
            </Grid>
            <Grid item xs={12} sm>
              <TextField fullWidth label="Descrição"/>
            </Grid>
          </Grid>
        </InputGroup>
        <Divider />
        <InputGroup title="Dados físicos">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={5}>
              <TextField
                fullWidth
                label="Comprimento do vão (eixo a eixo)"
                InputProps={{
                  endAdornment: <InputAdornment position="end">m</InputAdornment>
                }}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControl fullWidth>
                <InputLabel>Cabo</InputLabel>
                <Select value={0}>
                  <MenuItem value={0}>QUAIL - 2/0 AWG</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField fullWidth label="No. cond." />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={4}>
              <Box display="flex">
                <Checkbox checked={true} value={1} />
                <TextField
                  fullWidth
                  label="Com desnível"
                  InputProps={{
                    endAdornment: <InputAdornment position="end">m</InputAdornment>
                  }}
                />
              </Box>
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                fullWidth
                label="Largura da viga"
                InputProps={{
                  endAdornment: <InputAdornment position="end">m</InputAdornment>
                }}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                fullWidth
                label="Altura do pórtico mais alto"
                InputProps={{
                  endAdornment: <InputAdornment position="end">m</InputAdornment>
                }}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={4}>
              <TextField
                fullWidth
                label="Peso da cadeia"
                InputProps={{
                  endAdornment: <InputAdornment position="end">kg</InputAdornment>
                }}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                fullWidth
                label="Comprimento da cadeia"
                InputProps={{
                  endAdornment: <InputAdornment position="end">m</InputAdornment>
                }}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                fullWidth
                label="Porcentagem do vão"
                InputProps={{
                  endAdornment: <InputAdornment position="end">%</InputAdornment>
                }}
              />
            </Grid>
          </Grid>
        </InputGroup>
        <Divider />
        <InputGroup title="Condições do caso">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Esforço horizontal"
                InputProps={{
                  endAdornment: <InputAdornment position="end">kgf</InputAdornment>
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Temperatura máxima do vento"
                InputProps={{
                  endAdornment: <InputAdornment position="end">ºC</InputAdornment>
                }}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Temperatura mínima"
                InputProps={{
                  endAdornment: <InputAdornment position="end">ºC</InputAdornment>
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Velocidade máxima do vento"
                InputProps={{
                  endAdornment: <InputAdornment position="end">km/h</InputAdornment>
                }}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Temperatura média"
                InputProps={{
                  endAdornment: <InputAdornment position="end">ºC</InputAdornment>
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Velocidade média do vento"
                InputProps={{
                  endAdornment: <InputAdornment position="end">km/h</InputAdornment>
                }}
              />
            </Grid>
          </Grid>
        </InputGroup>
        <Divider />
        <Container>
          <Box py={2}>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Box display="flex" justifyContent="center" alignItems="center">
                  <Button variant="contained" color="primary">Calcular</Button>
                </Box>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Box display="flex" justifyContent="flex-end" alignItems="center">
                  <TextField
                    label="Temperatura"
                    InputProps={{
                      endAdornment: <InputAdornment position="end">ºC</InputAdornment>
                    }}
                  />
                  <Box ml={2}>
                    <Button
                      variant="contained"
                      color="primary"
                      startIcon={<ShowChart />}
                    >
                      Mostrar flecha
                    </Button>
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
        <Divider />
        <InputGroup title="Tabela de esticamento">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={8}>
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell>Temp. (ºC)</TableCell>
                    <TableCell>Flecha (m)</TableCell>
                    <TableCell>F. Hor. (kgf)</TableCell>
                    <TableCell>F. Vert. (kgf)</TableCell>
                    <TableCell>% do Vão</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {Array(5).fill(null).map(() => (
                    <TableRow>
                      {Array(5).fill(null).map(() => (
                        <TableCell></TableCell>
                      ))}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Typography>Fator de esforço inicial</Typography>
              <Slider
                defaultValue={1}
                marks
                step={1}
                min={-5}
                max={5}
                valueLabelDisplay="auto"
              />
              <Divider />
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="primary"
                  startIcon={<Check />}
                  className={classes.actionButton}
                >
                  Verificar suportab.
                </Button>
              </Box>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Button
                variant="contained"
                color="primary"
                startIcon={<Description />}
                className={classes.actionButton}
              >
                Abrir caso
              </Button>
              <Button
                variant="contained"
                color="primary"
                startIcon={<Save />}
                className={classes.actionButton}
              >
                Salvar caso
              </Button>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SettingsInputComponent />}
                className={classes.actionButton}
              >
                Incluir cabos
              </Button>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SaveAlt />}
                className={classes.actionButton}
              >
                Exportar para Excel
              </Button>
            </Grid>
          </Grid>
        </InputGroup>
      </>
    </>
  );
};

export default Home;
